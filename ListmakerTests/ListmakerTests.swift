//
//  ListmakerTests.swift
//  ListmakerTests
//
//  Created by Lewis Malin on 05/10/2016.
//  Copyright © 2016 Lewis Malin. All rights reserved.
//

import XCTest

class ListmakerTests: XCTestCase {
    
 
    
        

    
    override func setUp(){
        let lister = Lister.sharedInstance
        super.setUp()
        lister.clearList()
    }
        
    func testAddItem(){
        let lister = Lister.sharedInstance
        lister.add(item: "Bread")
        do {
            let newItem:String = try lister.getItem(atIndex: 0)
            XCTAssertEqual(newItem, "Bread")

        } catch {
            XCTFail()
        }
        
        
    }
    
    func testGetInvalidIndex() {
        let lister = Lister.sharedInstance
        lister.add(item: "Bread")
        lister.add(item: "Butter")
        print(lister.count)
        XCTAssertEqual(lister.count, 2)
        do {
            _ =  try lister.getItem(atIndex: 2)
            XCTFail()
        }
        catch ListError.outOfRange(let index){
            print("index \(index) is out of range")
        }
        catch{
            print("an error has occurred")
        }
    }
    
    
    func testClearList() {
        let lister = Lister.sharedInstance
        lister.add(item: "Bread")
        lister.add(item: "Butter")
        print(lister.count)
        XCTAssertEqual(lister.count, 2)
        lister.clearList()
        XCTAssertEqual(lister.count, 0)
    }
    
    func testInsertcorrect() {
        let lister = Lister.sharedInstance
        lister.add(item: "Chicken")
        lister.add(item: "Bread")
        
        do {
            try lister.insert(newElement: "Cheese", at: 1)
            let newItem:String = try lister.getItem(atIndex: 1)
            XCTAssertEqual(newItem, "Cheese")
            let newItem2:String = try lister.getItem(atIndex: 2)
            XCTAssertEqual(newItem2, "Bread")
        } catch {
            XCTFail()
        }
    }
    
    func testTooLow(){
        let lister = Lister.sharedInstance
        lister.add(item: "Chicken")
        lister.add(item:"Bread")
        
        do{
            try lister.insert(newElement: "Cheese", at: -1)
            
        }
        catch ListError.outOfRange(let index){
            print("index \(index) is out of range")
            
        }
        catch {
            XCTFail()
        }
    }
    
    
    func testInvalidFrom(){
        let lister = Lister.sharedInstance
        lister.add(item: "Chicken")
        lister.add(item:"Bread")
        
        do{
            try lister.moveItem(fromIndex: 8, toIndex: 0)
            
        }
        catch ListError.outOfRange(let index){
            print("index \(index) is out of range")
            
        }
        catch {
            XCTFail()
        }
    }
    
    func testInvalidTo(){
        let lister = Lister.sharedInstance
        lister.add(item: "Chicken")
        lister.add(item:"Bread")
        
        do{
            try lister.moveItem(fromIndex: 0, toIndex: 8)
            
        }
        catch ListError.outOfRange(let index){
            print("index \(index) is out of range")
            
        }
        catch {
            XCTFail()
        }
    }
    
    
    
    func testMoveCorrect(){
        let lister = Lister.sharedInstance
        lister.add(item: "Chocolate")
        lister.add(item: "Burrito")
        do{
            try lister.moveItem(fromIndex: 0, toIndex: 1)
            XCTAssertEqual(try lister.getItem(atIndex: 0), "Burrito")
        }
        catch{
            XCTFail()
        }
    }
    
    
    
    func testRemoveCorrect(){
        let lister = Lister.sharedInstance
        lister.add(item: "Chocolate")
        lister.add(item:"Burrito")
        do{
           try lister.remove(at: 0)
            XCTAssertEqual(try lister.getItem(atIndex: 0), "Burrito")
        }
        catch{
            XCTFail()
        }
    }
    
    func testRemoveOutOfRange(){
        let lister = Lister.sharedInstance
        lister.add(item: "Chicken")
        lister.add(item:"Bread")
        
        do{
            try lister.remove(at: -1)
        
        }
        catch ListError.outOfRange(let index){
            print("index \(index) is out of range")
            
        }
        catch {
            XCTFail()
        }
    }

    
    func testEmptyString() {
        let lister = Lister.sharedInstance
        lister.add(item: "Chicken")
        lister.add(item: "Cheese")
        
        do {
            try lister.insert(newElement: "", at: 1)
        }
        catch ListError.emptyString{
            print("cannot add an empty string")
        }
        catch{
            XCTFail()
        }
        
    }
    
    
    func testDuplicateItem(){
        let lister = Lister.sharedInstance
        lister.add(item: "Chicken")
        lister.add(item: "Dirt")
        
        do{
        try lister.insert(newElement: "Dirt", at: 0)
            XCTAssertNotEqual(try lister.getItem(atIndex: 0), try lister.getItem(atIndex: 2))
        }
        catch ListError.duplicateItem{
            print("cannot add duplicate item")
        }
        catch{
            XCTFail()
        }
    }
    
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
