//
//  Lister.swift
//  Listmaker
//
//  Created by Lewis Malin on 07/10/2016.
//  Copyright © 2016 Lewis Malin. All rights reserved.
//

import Foundation

enum ListError: Error{
    case emptyString
    case duplicateItem
    case outOfRange(index: Int)
}
/**
 Class to maintain shopping lists.
 
 
 */
 
public class Lister{
    
    var items:[String]
    
    
    public static let sharedInstance = Lister()
    
   private init(){
        self.items = []
    }
    
    
    
    /**
     Adds an Item to the list.
     ```
     let lister = Lister()
     lister.add(item: "Generic Item")
     ```
     - Parameter item: The item to add
     
     */
    public func add(item: String){
        self.items.append(item)
    }
    
    /**
    Retrieves a list item from the specified index
    ```
    let lister = Lister()
    let item = lister.getItem(atIndex: 2)
    ```
    - Parameter atIndex: The list index to retrieve from.
    - Returns: The list item
    - Throws: `ListError.outOfRange` if the index does not match a list item.
    */
    
    public func getItem(atIndex: Int) throws -> String{
        if (atIndex < 0) || (atIndex > (self.items.count - 1)){
            throw ListError.outOfRange(index: atIndex)
        }
        return self.items[atIndex]
    }
    
    
    /**
     An integer detailing the number of items within the list.
     
    */
    public var count:Int{
        get {
            return self.items.count
        }
    }
    
    
    /**
    Clears the list of all items
    ```
     let lister = Lister()
     lister.clearList()
     ```
    */
    public func clearList(){
        self.items.removeAll()
        
    }
    
    /**
    Inserts a new element into the list
    ```
    let lister = Lister()
    lister.insert(newElement:"aThing", at:2)
    ```
    - Parameters:
        - newElement: the element to add.
        - at: the index to add it at.

     - Throws: `ListError.duplicateItem` if the item is a duplicate of an item in the list
                `ListError.EmptyString` if the string entered is empty
                `ListError.outOfRange` if the index to insert is bigger than the list.
     
     */
    public func insert(newElement:String, at:Int) throws{
        if (at < 0 || at > self.items.count-1){
            throw ListError.outOfRange(index: at)
        }
        else if ( newElement == ""){
            throw ListError.emptyString
        }
        for index in 0...self.items.count-1{
            if (self.items[index] == newElement){
                throw ListError.duplicateItem
            }
            
            }
        
           self.items.insert(newElement, at: at)
            
        
        
        
    }
    
    
    
    
    /**
    Removes an element from the list.
     
     ```let lister = Lister()
     lister.insert(newElement:"aThing", at: 1)```
     lister.delete(at:1)
     - Parameter: at: the index to delete from
     - Throws: `ListError.outOfRange` if the index doesn't match a list item.
    */
    public func remove(at:Int) throws{
        if (at < 0 || at > self.items.count-1){
            throw ListError.outOfRange(index: at)
        }
        self.items.remove(at: at)
    }
    
    
    /**
     Moves an element from one position to another.
     
     ```let lister = Lister()
        lister.insert(newElement:"aThing", at: 1)
        lister.moveItem(fromIndex: 1, toIndex: 3
     ```
     - Parameters:
        - fromIndex: the index of the item you want to move.
        - toIndex: the index where you wish to move it to.
     - Throws: `ListError.outOfRange` if the index does not match a list item.
     
    */
    public func moveItem(fromIndex: Int, toIndex: Int) throws{
        if (fromIndex < 0 || fromIndex > self.items.count-1){
            throw ListError.outOfRange(index: fromIndex)
        }
        if (toIndex < 0 || toIndex > self.items.count-1){
            throw ListError.outOfRange(index: toIndex)
        }
        

        let item:String = self.items[fromIndex]
        self.items.remove(at: fromIndex)
        self.items.insert(item, at: toIndex)
    }
}



